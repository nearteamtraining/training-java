
import java.util.ArrayList;
import java.util.List;

public class Test1 {

    interface Form {
        String dessiner();
    }

    class Cercle implements Form {
        @Override
        public String dessiner() {
            return "Cercle";
        }
    }

    class Triangle implements Form {

        @Override
        public String dessiner() {
            return "Triangle";
        }
    }

    class Ellipse extends Cercle {

        @Override
        public String dessiner() {
            return super.dessiner() + "-Elliptique";
        }
    }

    void runTest1() {
        List<Form> forms = new ArrayList<>();

        forms.add(new Cercle());
        forms.add(new Cercle());
        forms.add(new Ellipse());
        forms.add(new Triangle());
        for (Form form:forms) {
            System.out.println(form.dessiner());
        }
    }

    public static void main(String[] args) {
        Test1 myTest = new Test1();
        myTest.runTest1();
    }
}
