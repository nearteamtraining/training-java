
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestCollection {

    class Client {
        public Client(String name, String lastname, ClientId id) {
            this.name = name;
            this.lastname = lastname;
            this.id = id;
        }

        private String name;
        private String lastname;

        public ClientId getId() {
            return id;
        }

        private ClientId id;

        public String getName() {
            return name;
        }

        public String getLastname() {
            return lastname;
        }
    }

    class ClientId {
        public ClientId(int clientNumber) {
            this.clientNumber = clientNumber;
        }

        private int clientNumber;
    }


    void runTestList() {
        Client client1 = new Client("john", "smith", new ClientId(1));
        Client client2 = new Client("john", "smith", new ClientId(1));
        List<Client> clients = new ArrayList<>();

        clients.add(client1);
        clients.add(client2);

        for (Client client : clients) {
            System.out.println("client - > " + client);
        }

    }

    void runTestMap() {
        Client client1 = new Client("john", "smith", new ClientId(1));
        Client client2 = new Client("john", "smith", new ClientId(2));
        Map<ClientId, Client> clients = new HashMap<>();

        clients.put(client1.getId(), client1);
        clients.put(client2.getId(), client2);

        System.out.println("client 1 is found ? - > " + clients.get(new ClientId(1)));

    }

    void runTestStream() {
        Client client1 = new Client("john", "smith", new ClientId(1));
        Client client2 = new Client("camilia", "smith", new ClientId(2));
        Client client3 = new Client("William", "legrand", new ClientId(3));
        Client client4 = new Client("Sylvie", "legrand", new ClientId(4));
        Client client5 = new Client("Andre", "lepetit", new ClientId(5));
        Client client6 = new Client("Sylviane", "legrand", new ClientId(6));
        Client client7 = new Client("Philippe", "smith", new ClientId(7));
        Client client8 = new Client("Anis", "Bessa", new ClientId(8));
        Client client9 = new Client("Mohamed", "Omrane", new ClientId(9));

        //Travail à faire
        //Filtrez les clients qui ont comme lastname smith et retournez la liste de leur Ids
        //Utilisez les stream java8

    }

    public static void main(String[] args) {
        TestCollection myTest = new TestCollection();
        myTest.runTestList();
        myTest.runTestMap();
    }
}
