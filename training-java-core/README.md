# training-java-core

Prérequis :

+ clonez le projet via git en utilisant la commande suivante : git clone https://bitbucket.org/nearteamtraining/training-java-core/
+ créez une branche git à votre nom à partir de la branche master : git checkout -b myname-nearteam-java-test
+ Vous avez besoin d'installer sur votre poste le JDK 8 et maven 3.5.2 au moins
+ Vous commiterez votre travail sur votre branche.
+ Vous n'avez pas besoin de vous créer un compte sur Bitbucket. Vous utilisez le user nttraining.


L'exercice :

+ Dans le respertoire src/main/resources nous avons un fichier qui contient les informations d'une liste de clients. Un client
est décrit par son Nom, prenom, date de naissance, date de création (Datetime). Les clients sont modélisés par la classe Client.
+ L'objectif est de créer un programme qui se lance avec une classe main qui lit tous les clients et
qui les écrit dans 2 fichiers, le premier au format XML et le deuxième au format Json. Le programme se lance de
la manière suivante :
  -  >java ClientRunner C:/path-to-output-dir
+ La classe main est la classe ClientRunner. Elle prend en paramètre le répertoire dans lequel
nous allons écrire les fichier d'outputs clients.xml et clients.json.
+ Nous vous encourageons à délimiter les responsabilités et à prévilégier la programmation par interface :
   - Vous pouvez créer l'interface ClientLoader avec son implémentation ClientTextFileLoader.
   - Vous pouvez faire le même principe pour l'écriture des fichiers dans les 2 formats cible avec l'interface ClientWriter.
+ Nous vous encourageons à utiliser Spring pour gérer l'injection des dépendances
+ Vous pouvez aussi utiliser le pattern singleton por gérer les différentes instantes des classes de service (Loader et Writer)
que vous pourrez créer.
+ Votre programme doit être évolutif. L'une des évolution que nous prévoyons est d'écrire les clients dans une base de données plutôt que dans des fichiers.
+ Si vous souhaitez implémenter cette partie avec une base H2, ce serait un plus.
+ Toutes les dépendances doivent être gérées via maven.


Pour toute information, n'hésitez pas à m'écrire : anis.bessa@nearteam.fr

